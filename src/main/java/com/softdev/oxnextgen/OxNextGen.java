/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.softdev.oxnextgen;

import java.util.Scanner;

/**
 *
 * @author slmfr
 */
public class OxNextGen {
    
    static char Player1 = 'X';
    static char Player2  = 'O';
    static char[][] board = new char[3][3];
    static char turn = Player1;
    static Scanner kb = new Scanner(System.in);
    
    
    
    static void createBoard(){
        for(int row = 0; row < 3 ;row++){
            for(int col = 0 ; col < 3 ;col++){
                board[row][col] = '-';
                
            }
        }
        
    }
    
    static void showBoard(){
        for(int row = 0; row < 3 ;row++){
            System.out.println(" -------------");
            for(int col = 0 ; col < 3 ;col++){
                if(col%1 ==  0){
                    System.out.print(" | ");
                }
                System.out.print(board[row][col]);
            }
            System.out.println(" | ");
        }
        System.out.println(" -------------");
    }
    
    static void  printTurn(){
        System.out.println("Turn " + turn + " move");
    }
    
    static void  printInputRowCol(){
        System.out.println("input row and col : ");
    }
    
    static int inputRow(){
        int Row = kb.nextInt();
        return  Row-1;
    }
    
    static int inputCol(){
        int Col = kb.nextInt();
        return  Col-1;
    }
    
    static boolean isValidMove(int row ,int col){
        return row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-';
    }
    
    static void makeMove(int row,int col){
        board[row][col] = turn;
    }
    
    static boolean isWin(){
        return RowWin() || ColWin() || Diagonals();  
    }
    
    static boolean RowWin(){
        for (int row = 0; row < 3; row++) {
            if (board[row][0] == turn && board[row][1] == turn && board[row][2] == turn) {
                return true;
            }
        }
        return false;
    }
    
    static boolean ColWin(){
        for (int col = 0; col < 3; col++) {
            if (board[0][col] == turn && board[1][col] == turn && board[2][col] == turn) {
                return true;
            }
        }
        return  false;
    }
    
    static boolean Diagonals(){
                
        return (board[0][0] == turn && board[1][1] == turn && board[2][2] == turn) || (board[0][2] == turn && board[1][1] == turn && board[2][0] == turn);
    }
    
    static void printPlayerWin(){
        System.out.println( "Player "+ turn + " wins!");
    }
    
    static void printRestart(){
        System.out.println("input 1 to restart or 2 to exit :");
    }
    
    static int inputRestart(){
        int input = kb.nextInt();
        return  input;
    }
    
    static boolean isRestart(int num){
        return  num == 1;
    }
    
    static boolean isBoardFull(){
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (board[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    static void printDraw(){
        System.out.println("It's a draw!");
    }
    
    static void printNope(){
        System.out.println("Nope");
    }
    
    static void swtichTurn(){
        if(turn == Player1){
            turn = Player2;
        }else{
            turn = Player1;       
        }
    }

    public static void main(String[] args) {
        createBoard();
        while (true) {            
            showBoard();
            printTurn();
            printInputRowCol();
            int Row = inputRow();
            int Col = inputCol();
            if(isValidMove(Row, Col)){
                makeMove(Row, Col);
                if(isWin()){
                    showBoard();
                    printPlayerWin();
                    printRestart();
                    int menu = inputRestart();
                    if(isRestart(menu)){
                        createBoard();
                        continue;
                    }else{
                        break;
                    }
                }
                if(isBoardFull()){
                    showBoard();
                    printDraw();
                    printRestart();
                    int menu = inputRestart();
                    if(isRestart(menu)){
                        createBoard();
                        continue;
                    }else{
                        break;
                    }
                }
                swtichTurn();
            }else{
                printNope();
            }
            }
        }
}
